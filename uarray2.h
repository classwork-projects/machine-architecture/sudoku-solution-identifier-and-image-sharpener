#ifndef A2_LIBRARY_H
#define A2_LIBRARY_H

typedef struct UArray2_T *UArray2_T;
extern UArray2_T UArray2_new (int width, int height, int size); // creator
extern void UArray2_free(UArray2_T array); // destroyer
extern unsigned int UArray2_width(UArray2_T array); // observer
extern unsigned int UArray2_height(UArray2_T array); // observer
extern int UArray2_size (UArray2_T array); // observer
extern void *UArray2_at(UArray2_T array, int x, int y); // observer
extern void UArray2_map_row_major(UArray2_T array, void apply(void *value, int x, int y));
extern void UArray2_map_col_major(UArray2_T array, void apply(void *value, int x, int y));

#endif //A2_LIBRARY_H
