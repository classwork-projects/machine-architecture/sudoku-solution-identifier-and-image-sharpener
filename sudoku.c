#include <stdio.h>
#include <stdlib.h>
#include "pnmrdr.h"
#include <array.h>
#include <uarray2.h>
#include <math.h>

//Returns 2d array that contains image data
UArray2_T readFile(FILE *pgmFile) {
    Pnmrdr_T rdr;
    Pnmrdr_mapdata data;
    TRY
        rdr = Pnmrdr_new(pgmFile);
        data = Pnmrdr_data(rdr);

    //Possible Exceptions
    EXCEPT(Pnmrdr_Count)
        fclose(pgmFile);
        exit(EXIT_FAILURE);

    EXCEPT(Pnmrdr_Badformat)
        fclose(pgmFile);
        exit(EXIT_FAILURE);

    END_TRY;

    if ((data.type != Pnmrdr_gray) || data.width != 9 || data.height != 9) //not a gray pgm image or not the correct board dimensions
    {
        fclose(pgmFile);
        Pnmrdr_free(&rdr);
        exit(EXIT_FAILURE);
    }

    UArray2_T board = UArray2_new(9,9, sizeof(int)); // Create new 2d array to store board
    int* current_pixel;
    //Add each pixels intensity to 2d board array
    for (unsigned int j = 0; j < data.height; j++) {
        for (unsigned int i = 0; i < data.width; i++) {
            current_pixel = UArray2_at(board, i, j);
            *current_pixel = Pnmrdr_get(rdr);
            if(*current_pixel < 1 || *current_pixel > 9) //Check if every intensity is within range
            {
                UArray2_free(board);
                Pnmrdr_free(&rdr);
                exit(EXIT_FAILURE);
            }
        }
    }
    //Average_Brightness = (Average_Brightness / Pixel_Count) / data.denominator; //Compute Average Brightness
    Pnmrdr_free(&rdr);
    return board;
}

//Determine if 9 int array contains 1-9
void validNine(Array_T arr, UArray2_T board)
{
    int contains;
    int* current;
    for(int j=1; j<10; j++) //Ints to test if contained in array
    {
        contains = 0;
        for(int i=0; i<9; i++)
        {
            current = Array_get(arr, i); //Current index in array
            if (*current == j)
            {
                contains = 1; //int was found in array
                break;
            }
        }
        if(contains != 1) //Not in array
        {
            for(int h=0; h<9; h++)
            {
                current = Array_get(arr, h);
            }
            UArray2_free(board);
            Array_free(&arr);
            exit(EXIT_FAILURE);
        }

    }
}

//Determines if all rows on board contain 1-9
void checkRows(UArray2_T board)
{
    Array_T current_row = Array_new(9, sizeof(int));
    int* current_index; //index in array
    int* current_point; //point in 2d array

    //Determine if row is correct
    for(int j=0; j<9; j++)
    {
        for(int i=0; i<9; i++)
        {
            current_index = Array_get(current_row, i); //Current index in array
            current_point = UArray2_at(board,i,j); //Corresponding index in 2d board array
            *current_index = *current_point; //Set array value
        }
        validNine(current_row,board);

    }
    Array_free(&current_row);
}

//Determines if all columns on board contain 1-9
void checkCols(UArray2_T board)
{
    Array_T current_col = Array_new(9, sizeof(int));
    int* current_index; //index in array
    int* current_point; //point in 2d array

    //Determine if col is correct
    for(int i=0; i<9; i++)
    {
        for(int j=0; j<9; j++)
        {
            current_index = Array_get(current_col, j); //Current index in array
            current_point = UArray2_at(board,i,j); //Corresponding index in 2d board array
            *current_index = *current_point; //Set array value
        }
        validNine(current_col,board);

    }
    Array_free(&current_col);
}

//Determines if the 9 3*3 squares on board contain 1-9
void checkSquares(UArray2_T board)
{
    Array_T current_square = Array_new(9, sizeof(int));
    int* current_index; //index in array
    int* current_point; //point in 2d array

    //9 squares
    for(int n=0; n<9; n++)
    {
        for(int j=0; j<3; j++)
        {
            for(int i=0; i<3; i++)
            {
                current_index = Array_get(current_square, (j*3)+i); //Current index in array
                current_point = UArray2_at(board, ((3*n)%9)+i, ((int)floor(n/3)*3)+j); //Corresponding index in 2d board array
                *current_index = *current_point; //Set array value
            }
        }
        validNine(current_square,board);
    }
    Array_free(&current_square);
}

int main(int argc, char* argv[]){
    FILE *pgmfile;
    if (argc == 1) //Run with standard input
    {
        pgmfile = stdin;
        if(pgmfile == NULL)
        {
            exit(EXIT_FAILURE);
        }
    }
    else if (argc > 2)
    {
        exit(EXIT_FAILURE);
    }
    else //Run with pgm file
    {
        pgmfile = fopen(argv[1], "rb"); //Opens File
        if (pgmfile == NULL) {
            exit(EXIT_FAILURE);
        }
    }
    UArray2_T board = readFile(pgmfile); //Converts pgm file into a 2d array
    fclose(pgmfile); //Closes File

    checkSquares(board);
    checkRows(board);
    checkCols(board);

    UArray2_free(board);
}