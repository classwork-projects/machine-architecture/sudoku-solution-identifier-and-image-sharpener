# Sudoku Solution Identifier and Image Sharpener
This project was developed to study the principle of locality and interface design.
<br />

## UArray2
A 2D array interface with indexing and mapping.
<br />

## Bit2
A 2D array interfacing for storing bits with indexing and mapping. Each element in the array is a single bit.
<br />

## Sudoku
Input is given as a single portable graymap ﬁle through the command line or standard input. 
If the graymap ﬁle represents a solved sudoku puzzle, the program calls exit(0); otherwise it calls exit(1).  

A solved sudoku puzzle is a nine-by-nine graymap with these properties: 
- The maximum pixel intensity (aka the denominator for scaled integers) is nine. 
- No pixel has zero intensity. - In each row, no two pixels have the same intensity. 
- In each column, no two pixels have the same intensity. 
- When then nine-by-nine graymap is divided into nine three-by-three submaps (like a tic-tac-toe board), in each three-by-three submap, no two pixels have the same intensity.
<br />

## Image Sharpener
Given a scanned image as a portable bitmap file, unblackedges will removes the image's black edges to improve clarity.
<br />

Before                     |  After
:-------------------------:|:-------------------------:
![Scanned image before unblackedges](before.png)  |  ![Scanned image after unblackedges](after.png)
<br />

**Input**  
At most one argument: 
- If an argument is given, it should be the name of a portable bitmap ﬁle (in pbm format).
- If no argument is given, unblackedges reads from standard input, which should contain a portable bitmap. 
<br />

**Output**  
A portable bitmap ﬁle on standard output which is identical to the original ﬁle except that all black edge pixels are changed to white.
