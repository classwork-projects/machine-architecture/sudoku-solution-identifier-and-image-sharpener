#include <stdio.h>
#include <stdlib.h>
#include "pnmrdr.h"
#include "array.h"
#include "stack.h"
#include <assert.h>
#include <uarray2.h>
#include <bit2.h>

//Pixel Object
struct Pixel{
    unsigned x,y; //holds coordinates in the 2d vector
    int value; //holds the bit value
    int edge; //1 if edge pixel, 0 if not an edge or not yet tested
};

//Returns 1 if the given pixel has an edge pixel neighbor
int edgeNeighbor(UArray2_T image_copy, struct Pixel* pixel )
{
    struct Pixel* potential_neighbor;

    //up
    if(pixel->y >0) //within bounds
    {
        potential_neighbor = UArray2_at(image_copy, pixel->x, pixel->y - 1); //neighbor above of the given pixel
        if (potential_neighbor->edge == 1) //test if edge
            return 1;
    }
    //down
    if(pixel->y < UArray2_height(image_copy)-1)  //within bounds
    {
        potential_neighbor = UArray2_at(image_copy, pixel->x, pixel->y + 1); //neighbor below of the given pixel
        if (potential_neighbor->edge == 1) //test if edge
            return 1;
    }
    //left
    if(pixel->x >0)  //within bounds
    {
        potential_neighbor = UArray2_at(image_copy, pixel->x - 1, pixel->y); //neighbor to the left of the given pixel
        if (potential_neighbor->edge == 1) //test if edge
            return 1;
    }
    //right
    if(pixel->x < UArray2_width(image_copy)-1) //within bounds
    {
        potential_neighbor = UArray2_at(image_copy, pixel->x + 1, pixel->y); //neighbor to the right of the given pixel
        if (potential_neighbor->edge == 1) //test if edge
            return 1;
    }
    return 0; //no edge neighbors
}

//Returns a 2d Array of struct Pixel objects from the given 2d bit vector
UArray2_T copyToArray(Bit2_T image)
{
    struct Pixel* current_pixel;
    UArray2_T image_copy = UArray2_new(Bit2_width(image),Bit2_height(image), sizeof(struct Pixel)); //new 2d array to hold struct pixel objects

    //Loop through every pixel
    for (int y = 0; y < Bit2_height(image); y++)
    {
        for (int x = 0; x < Bit2_width(image); x++)
        {
            current_pixel = UArray2_at(image_copy,x,y); //point to correct pixel object in 2d array
            //set values
            current_pixel->x = x;
            current_pixel->y = y;
            current_pixel->value = Bit2_get(image,x,y);
            //if pixel is on one of the edges of the image set edge to 1
            if(y==0 || y==Bit2_height(image)-1 || x==0 || x==Bit2_width(image)-1)
                current_pixel->edge = 1;
            else
                current_pixel->edge = 0;
        }
    }
    return image_copy;
}


//Function to print bit array in PBM format to stdout
void printImage(Bit2_T image)
{
    fprintf(stdout,"P1 \n");
    fprintf(stdout, "%d %d \n", Bit2_width(image), Bit2_height(image));
    for (int y = 0; y < Bit2_height(image); y++) {
        for (int x = 0; x < Bit2_width(image); x++) {
            fprintf(stdout,"%d ", Bit2_get(image, x, y));
        }
        if (y < Bit2_height(image) - 1) {
            fprintf(stdout,"\n");
        }
    }
}

//Remove edges from given image and return it
void unblackEdges(Bit2_T image, UArray2_T image_copy)
{
    struct Pixel* current_pixel;
    int prev;

    //Must loop through 2d array 4 times, (top down) (bottom up) (left to right) (right to left), to white out all edge pixels correctly

    //Row Down lr
    //Loop through the 2d array from the top row down and find pixels that have edge neighbors, white them out, and set them as edge pixels
    for (int y = 0; y < Bit2_height(image); y++)
    {
        for (int x = 0; x < Bit2_width(image); x++)
        {
            if (Bit2_get(image, x, y) == 1)//If the pixel is black
            {
                current_pixel = UArray2_at(image_copy, x,y);
                if(current_pixel->edge==1 || edgeNeighbor(image_copy,current_pixel)==1) //If pixel is already an edge pixel or is neighbored to one
                {
                    current_pixel->edge = 1;
                    prev = Bit2_put(image,x,y,0); //white out the pixel
                    if(prev==0)
                    {
                        fprintf(stderr,"Tried to White Out a White Pixel");
                    }
                }

            }
        }
    }
    //Row UP lr
    //Loop through the 2d array from the bottom row up and find pixels that have edge neighbors, white them out, and set them as edge pixels
    for (int y = Bit2_height(image)-1; y >= 0; y--)
    {
        for (int x = 0; x < Bit2_width(image); x++)
        {
            if (Bit2_get(image, x, y) == 1)//If the pixel is black
            {
                current_pixel = UArray2_at(image_copy, x,y);
                if(current_pixel->edge==1 || edgeNeighbor(image_copy,current_pixel)==1) //If pixel is already an edge pixel or is neighbored to one
                {
                    current_pixel->edge = 1;
                    prev = Bit2_put(image,x,y,0); //white out the pixel
                    if(prev==0)
                    {
                        fprintf(stderr,"Tried to White Out a White Pixel");
                    }
                }

            }
        }
    }
    //COL down rl
    //Loop through the 2d array from the right column to the left and find pixels that have edge neighbors, white them out, and set them as edge pixels
    for (int x = Bit2_width(image)-1; x >= 0; x--)
    {
        for (int y=0; y<Bit2_height(image); y++)
        {
            if (Bit2_get(image, x, y) == 1)//If the pixel is black
            {
                current_pixel = UArray2_at(image_copy, x,y);
                if(current_pixel->edge==1 || edgeNeighbor(image_copy,current_pixel)==1) //If pixel is already an edge pixel or is neighbored to one
                {
                    current_pixel->edge = 1;
                    prev = Bit2_put(image,x,y,0); //white out the pixel
                    if(prev==0)
                    {
                        fprintf(stderr,"Tried to White Out a White Pixel");
                    }
                }

            }
        }
    }
    //COL down lr
    //Loop through the 2d array from the left column to the right and find pixels that have edge neighbors, white them out, and set them as edge pixels
    for (int x = 0; x > Bit2_width(image)-1; x++)
    {
        for (int y=0; y<Bit2_height(image); y++)
        {
            if (Bit2_get(image, x, y) == 1)//If the pixel is black
            {
                current_pixel = UArray2_at(image_copy, x,y);
                if(current_pixel->edge==1 || edgeNeighbor(image_copy,current_pixel)==1) //If pixel is already an edge pixel or is neighbored to one
                {
                    current_pixel->edge = 1;
                    prev = Bit2_put(image,x,y,0); //white out the pixel
                    if(prev==0)
                    {
                        fprintf(stderr,"Tried to White Out a White Pixel");
                    }
                }

            }
        }
    }

}

//Returns 2d array that contains image data
Bit2_T readFile(FILE *pbmFile) {
    Pnmrdr_T rdr;
    Pnmrdr_mapdata data;
    TRY
        rdr = Pnmrdr_new(pbmFile);
        data = Pnmrdr_data(rdr);

    if (data.type != Pnmrdr_bit) { //not a bit map image
        fclose(pbmFile);
        fprintf(stderr, "Not a bit map image\n");
        Pnmrdr_free(&rdr);
        exit(EXIT_FAILURE);
    }


    //Possible Exceptions
    EXCEPT(Pnmrdr_Count)
        fclose(pbmFile);
        fprintf(stderr, "Count Error\n");
        exit(EXIT_FAILURE);

    EXCEPT(Pnmrdr_Badformat)
        fclose(pbmFile);
        fprintf(stderr, "Not an PBM Image\n");
        exit(EXIT_FAILURE);

    END_TRY;

    Bit2_T image = Bit2_new(data.width, data.height); // Create new 2d bit array to store image

    int current_bit;

    //Add each bit to 2d bit array
    for (unsigned int j = 0; j < data.height; j++) {
        for (unsigned int i = 0; i < data.width; i++) {
            current_bit = Pnmrdr_get(rdr);
            Bit2_put(image, i, j, current_bit);
        }
    }

    Pnmrdr_free(&rdr);
    return image;
}


int main(int argc, char* argv[]) {
    FILE *pbmfile;
    if (argc == 1) //Run with standard input
    {
        pbmfile = stdin;
        if (pbmfile == NULL)
        {
            fprintf(stderr, "Incorrect Format\n");
            exit(EXIT_FAILURE);
        }
    }
    else if (argc > 2)
    {
        fprintf(stderr, "Too many arguments\n");
        exit(EXIT_FAILURE);
    }
    else //Run with pbm file
    {
        pbmfile = fopen(argv[1], "rb"); //Opens File
        if (pbmfile == NULL)
        {
            fprintf(stderr, "%s: Could not open file %s for reading. Closing file and doing nothing.\n",argv[0], argv[1]);
            exit(EXIT_FAILURE);
        }
    }
    Bit2_T image = readFile(pbmfile); //read image to 2d bit vector
    fclose(pbmfile);

    UArray2_T image_copy;
    image_copy = copyToArray(image); //copy 2d bit vector to 2d array of pixel objects

    unblackEdges(image, image_copy); //whiten all edge pixels

    printImage(image);//print whitened image to stdout

    Bit2_free(image);
    UArray2_free(image_copy);
}
