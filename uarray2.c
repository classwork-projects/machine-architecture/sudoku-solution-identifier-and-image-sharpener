#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "mem.h"
#include "uarray2.h"
#include "array.h"

struct UArray2_T{
    int width;
    int height;
    int size;
    Array_T main_array; //Contains Height * Width elements
};

//Creates a new 2d array and returns a pointer to type UArray2_T
UArray2_T UArray2_new (int width, int height, int size)
{
    UArray2_T uarray2;
    NEW(uarray2);
    uarray2->width = width;
    uarray2->height = height;
    uarray2->size = size;

    Array_T array;

    //Create Array to hold Height * Width elements
    array = Array_new(height * width, size);
    uarray2->main_array = array;
    return uarray2;
}

//Frees the 2d array
extern void UArray2_free(UArray2_T uarray2)
{
    Array_free(&uarray2->main_array);
    FREE(uarray2);
}

//Returns the width of the 2d array as an int
extern unsigned int UArray2_width(UArray2_T uarray2)
{
    assert(uarray2);
    return uarray2->width;
}

//returns the height of the 2d array as an int
extern unsigned int UArray2_height(UArray2_T uarray2)
{
    assert(uarray2);
    return uarray2->height;
}

//returns the size in bytes of the 2d array elements as an int
extern int UArray2_size (UArray2_T uarray2)
{
    assert(uarray2);
    return(uarray2->size);
}

extern void *UArray2_at (UArray2_T uarray2, int x, int y) // observer
{
    int index = (y * uarray2->width) + x;
    return(Array_get(uarray2->main_array, index));
}

extern void UArray2_map_row_major(UArray2_T array, void apply(void *value, int x, int y))
{
    assert(apply);
    for (int h = 0; h < array->height; h++)
    {
        for (int w = 0; w < array->width; w++) {
            apply(UArray2_at(array, w, h), w, h);
        }
    }
}

extern void UArray2_map_col_major(UArray2_T array, void apply(void *value, int x, int y))
{
    assert(apply);
    for (int w = 0; w < array->width; w++) {
        {
            for (int h = 0; h < array->height; h++)
                apply(UArray2_at(array, w, h), w, h);
        }
    }
}
