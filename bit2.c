#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "mem.h"
#include <math.h>
#include "bit.h"
#include "bit2.h"

struct Bit2_T{
    int width;
    int height;
    Bit_T bit_vector; //Contains Height * Width bit elements
};

extern Bit2_T Bit2_new (int width, int height)
{
    Bit2_T bit2;
    NEW(bit2);
    bit2->width = width;
    bit2->height = height;

    Bit_T bitvector;

    assert(height>=0);
    assert(width>=0);

    //Creates a new bit vector that stores width by height elements
    bitvector = Bit_new(height*width);

    bit2->bit_vector = bitvector;
    return bit2;
}
extern void Bit2_free(Bit2_T bit2)
{
    Bit_free(&bit2->bit_vector);
    free(bit2);
}
extern int Bit2_width(Bit2_T bit2)
{
    assert(bit2);
    return bit2->width;
}
extern int Bit2_height(Bit2_T bit2)
{
    assert(bit2);
    return bit2->height;
}
//Returns bit at x,y coordinates
extern int Bit2_get(Bit2_T bit2, int x, int y)
{
    assert(bit2);
    assert(x >= 0 && y>=0);
    assert(x <= bit2->width && y<= bit2->height);

    int index = (y*bit2->width)+x;
    return (Bit_get(bit2->bit_vector,index));
}
extern int Bit2_put(Bit2_T bit2, int x, int y, int bit)
{
    assert(bit2);
    assert(bit == 0 || bit == 1);
    assert(x >= 0 && y>=0);
    assert(x <= bit2->width && y<= bit2->height);

    int index = (y*bit2->width)+x;

    int prev = Bit_put(bit2->bit_vector,index,bit);

    return prev;
}

extern void Bit2_map_row_major(Bit2_T bit2, void apply(int x, int y, int bit))
{
    assert(bit2);
    for(int h = 0; h < bit2->height; h++)
    {
        for(int w = 0; w < bit2->width; w++)
        {
            apply(w, h, Bit2_get(bit2, w, h));
        }
    }
}
extern void Bit2_map_col_major(Bit2_T bit2, void apply(int x, int y, int bit))
{
    assert(bit2);
    for(int w = 0; w < bit2->width; w++)
    {
        for(int h = 0; h < bit2->height; h++)
        {
            apply(w, h, Bit2_get(bit2, w, h));
        }
    }
}
