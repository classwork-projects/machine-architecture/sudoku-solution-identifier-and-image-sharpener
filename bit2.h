#ifndef A2_BIT2_H
#define A2_BIT2_H

//Define every instance of Bit2_T to represent a pointer to the structure Bit2_T
typedef struct Bit2_T *Bit2_T;
//Creates and returns a pointer to structure that contains a 2-D bit vector of given length and width
extern Bit2_T Bit2_new (int width, int height);
//Frees the 2-D bit vector and the structure that it is contained within
extern void Bit2_free(Bit2_T bit2);
//Returns the width of the 2-D bit vector contained within the Bit2_T structure
extern int Bit2_width(Bit2_T bit2);
//Returns the height of the 2-D bit vector contained within the Bit2_T structure
extern int Bit2_height(Bit2_T bit2);
//Returns the value of the bit at the x,y index inside of the 2d bit vector contained within the Bit2_T structure
extern int Bit2_get(Bit2_T bit2, int x, int y);
//Sets the bit value of the bit at the x,y index inside of the 2d bit vector contained within the Bit2_T structure to the value of bit and returns the previous value of that bit
extern int Bit2_put(Bit2_T bit2, int x, int y, int bit);
//Calls apply function on each pixel in the 2d bit vector in row major order
extern void Bit2_map_row_major(Bit2_T bit2, void apply(int x, int y, int bit));
//Calls apply function on each pixel in the 2d bit vector in column major order
extern void Bit2_map_col_major(Bit2_T bit2, void apply(int x, int y, int bit));

#endif //A2_BIT2_H
